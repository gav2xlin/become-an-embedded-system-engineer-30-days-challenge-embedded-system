#include <stdio.h>

int main(void)
{
    int a = 10, b = 20;
    int sum = a + b;

    printf("Sum: %d\n", sum); // 30

    return 0;
}
