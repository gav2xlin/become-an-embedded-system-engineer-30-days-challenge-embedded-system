// https://cplusplus.com/reference/cstdio/fgets/
// https://cplusplus.com/reference/cstring/strlen/
// https://cplusplus.com/reference/cstring/strcspn/

#include <stdio.h>
#include <string.h>

int main()
{
    char s[64];
    if (fgets(s, 64, stdin) != NULL) {
        s[strcspn(s, "\r\n")] = 0;
        // printf("string = %s length = %zu\n", s, strlen(s));
        int l = 0;
        for (int i = 0; i < sizeof(s) && s[i]; ++i, ++l) {}

        printf("string = %s length = %d\n", s, l);
    }

    return 0;
}