// https://cplusplus.com/reference/cstdio/fgets/
// https://cplusplus.com/reference/cstdio/scanf/

#include <stdio.h>

int main()
{
    char s[64];
    // scanf("%64[^\n]", s); // %s stopping at the first whitespace found
    if (fgets(s, 64, stdin) != NULL) {
        printf("%s\n", s);
    }

    return 0;
}
