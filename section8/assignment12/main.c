// https://cplusplus.com/reference/cstdlib/strtoul/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    char s[64];
    if (fgets(s, 64, stdin) != NULL) {
        unsigned long int d = strtoul(s, NULL, 10);
        printf("%lu", d);
    }

    return 0;
}
