// https://cplusplus.com/reference/cstdio/fgets/
// https://cplusplus.com/reference/cstdio/scanf/

#include <stdio.h>

int main()
{
    // char* greeting = "Hello";
    // char greeting[6] = {'H', 'e', 'l', 'l', 'o', '\0'};
    char greeting[] = {'H', 'e', 'l', 'l', 'o', '\0'};
    printf("Greeting message: %s\n", greeting);

    return 0;
}
