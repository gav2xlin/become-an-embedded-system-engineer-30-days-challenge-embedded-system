// https://cplusplus.com/reference/cstdio/scanf/

#include <stdio.h>
#include <string.h>

void main(void)
{
    char name[64];
    printf("Enter the name:");
    scanf("%64[^\n]s", name);
    // scanf("%[^\n]*c", name);
    // scanf("%64[^\n]*c", name);
    // scanf("%64[^\n]%*c", name);
    printf("name = %s len = %zu\n", name, strlen(name));
}
