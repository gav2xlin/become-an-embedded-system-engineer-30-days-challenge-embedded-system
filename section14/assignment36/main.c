// https://cplusplus.com/reference/cstring/strcmp/

#include <stdio.h>
#include <string.h>

int main()
{
    char animal[16];
    printf("animal (dog, crocodile, tortoise, snake): ");
    scanf("%16s", animal);

    // switch is used with integral types

    if (!strcmp(animal, "dog")) {
        printf("mammal");
    } else if (!strcmp(animal, "crocodile") || !strcmp(animal, "tortoise") || !strcmp(animal, "snake")) {
        printf("reptile");
    } else {
        printf("unknown");
    }

    return 0;
}
