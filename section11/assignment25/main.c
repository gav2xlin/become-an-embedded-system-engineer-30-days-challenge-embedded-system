#include <stdio.h>

int main()
{
    int a, b;
    printf("a: ");
    scanf("%d", &a);

    printf("b: ");
    scanf("%d", &b);

    printf("sum: %d\nproduct: %d\nsqr.sum: %d\n", a + b, a * b, a * a + b * b);

    return 0;
}
