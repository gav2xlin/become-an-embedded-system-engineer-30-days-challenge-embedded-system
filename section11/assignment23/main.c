// https://cplusplus.com/reference/cstdio/sscanf/
// https://cplusplus.com/reference/cstdio/fgets/

#include <stdio.h>

int main()
{
    double c;
    printf("Centigrade:");
    // scanf("%lf", &c);
    char s[32];
    fgets(s, sizeof(s), stdin);
    sscanf(s, "%lf", &c);

    double f = c * 1.8 + 32;
    printf("Fahrenheit: %lf", f);

    return 0;
}
