#include <stdio.h>

#define MIN_IN_HOUR 60

int main()
{
    unsigned int h, m;
    printf("hours: ");
    scanf("%u", &h);

    printf("minutes: ");
    scanf("%u", &m);

    printf("total: %u", h * MIN_IN_HOUR + m);

    return 0;
}
