// https://cplusplus.com/reference/cstdlib/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num;
    scanf("%d", &num);
    printf("num=%u", abs(num));

    return 0;
}
