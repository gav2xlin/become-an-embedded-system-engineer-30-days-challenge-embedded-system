#include <stdio.h>

int main()
{
    int p, q, r, s;

    p = 1;
    q = 2;
    r = p, q;
    s = (p, q); // the comma operator

    printf("p=%d q=%d", p, q); // 1, 2
    // printf("\nr=%d s=%d", r, s); // 1, 2

    return 0;
}
