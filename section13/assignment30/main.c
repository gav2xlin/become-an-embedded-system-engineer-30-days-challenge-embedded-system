#include <stdio.h>

int main()
{
    int x;
    printf("x: ");
    scanf("%d", &x);

    printf("the number is %szero\n", x != 0 ? "not " : "");

    return 0;
}
