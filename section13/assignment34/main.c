#include <stdio.h>

int main()
{
    int x, y, z;
    printf("x, y, z: ");
    scanf("%d%d%d", &x, &y, &z);

    // instead of if-else-if ladder
    /*int m = x > y ? x : y;
    m = m > z ? m : z;*/
    int m = x > y ? (x > z ? x : z) : (y > z ? y : z);

    printf("max: %d\n", m);

    return 0;
}
