#include <stdio.h>

int main()
{
    char s[64];

    int x, y;
    printf("x and y: ");
    /*fgets(s, sizeof(s), stdin);
    sscanf(s, "%d%d", &x, &y);*/
    scanf("%d%d", &x, &y);

    printf("numbers are %s\n", x == y ? "equal" : "not equal");

    return 0;
}
