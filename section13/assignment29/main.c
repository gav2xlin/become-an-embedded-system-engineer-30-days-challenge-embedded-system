#include <stdio.h>

int main()
{
    int x, y;
    printf("x: ");
    scanf("%d", &x);

    printf("y: ");
    scanf("%d", &y);

    printf("numbers are %s\n", x == y ? "equal" : "not equal");

    return 0;
}
